# 💻 Simulador de mensajes Syslog 📡

  - [Descripción](#descripción-page_facing_up)
  - [Tecnologias aplicadas](#tecnologias-aplicadas)
  - [Como ejecutar la aplicación:](#como-ejecutar-la-aplicación-wrench)
    - [Crear la maquina docker](#crear-la-maquina-docker-whale)
    - [Como ejecutar la aplicación](#como-ejecutar-la-aplicación-wrench)
      - [Desde Docker](#desde-docker-whale)
      - [Desde Postman](#desde-postman-running)
  - [Parámetros de la aplicación](#parámetros-de-la-aplicación-wrench)
  - [Máquina Servidor Rsyslog](#máquina-servidor-rsyslog-computer)
  - [Testing completo de la aplicación](#🧪-testing-completo-de-la-aplicación)
  - [Usuarios](#👤-usuarios)
  - [Pantallas](#💻-pantallas)
  - [Front con Angular](#🚀-front-con-angular-17)
    - [Como compilar angular](#como-compilar-angular)
  - [Endpoints](#endpoints)

## :page_facing_up: Descripción 

Este proyecto es un simulador de mensajes Syslog, que permite enviar mensajes a un servidor Rsyslog. La aplicación permite enviar mensajes con diferentes cabeceras y cuerpos, y permite enviar mensajes a diferentes velocidades.

## Tecnologias aplicadas

### 👁‍🗨 AWS 

He decidido utilizar aplicaciones en la nube para poder acceder desde cualquier lugar y que sea más facil el acceder a la aplicación desarrollada. Para las maquinas he utilizado EC2, que tienen la facilidad de instalar rapidamente maquinas con baja memoria.
Además, se utilizaron servicios adicionales como Elastic IP para poder tener una IP estatica externa o la configuración de las politicas de grupo para poder abrir y cerrar puertos especificos y mejorar la seguridad de la aplicación.

### 🐳  Docker

Además de AWS, he querido añadir docker para poder instanciar las máquinas en local y poder realizar testing cuando se desarrolla. Aunque solamente haya trabajado yo en este proyecto, la idea a gran escala es tener un equipo que itere sobre la aplicación, por lo que la mejor manera de testear un desarrollo si no se tienen ramas de development en git (o en su defecto, se comparten entre varios desarrolladores) es poder compilar el codigo en local y poder realizar pruebas.

### 🐘 PostgreSQL

En un principio dudé entre utilizar PostgreSQL o ElasticSearch, pero viendo los pocos datos que vamos a tener al principio me decanté por PostgreSQL. En un futuro, se podria implantar ElasticSearch para una futura iteración de la aplicación, realizando el guardado de los mensajes enviados en una maquina Elastic. Esto nos permitiria poder realizar una búsqueda de dichos mensajes y poder compararlos en otra ejecución distinta. Además, las máquinas de ElasticSearch consumen muchos más recursos y actualmente no se ve necesario para un desarrollo inicial.
La base de datos se utiliza basicamente para el guardado de usuarios y registros.

### ☕️  Java / Spring Boot / Spring Security

Mi mayor reto estaba en el core de la aplicación, en este caso en Java. Puesto que llevo muchos años desarrollando en Java, si que es cierto que siempre fueron tecnologias antiguas como Java 7. En el TFG he visto una oportunidad de actualizar mi conocimiento estudiando la manera de realizar las llamadas API REST junto con Spring Boot. También he incluido Spring Security ya que la finalidad de esta aplicación es la de simular los mensajes de un hardware para poder detecar ataques maliciosos, es por ello que he preferido incluir un login y que solo las personas autorizadas y con un token correcto puedan ejecutarla.

### 🔈Jacoco / JUnit / SonarCloud

Puesto que es una herramienta que me gustaria que el dia de mañana se utilizase a nivel de ciberseguridad, he decidido que tiene que ser una aplicación robusta y sin fallos. Es por ello que he querido dedicar un poco más de tiempo al testing, tanto con test de cobertura, como con los test unitarios en JUnit. Actualmente, el estado del proyecto es de A en todos las categorias y un 97,5% de cobertura en el codigo completo.

### 🆖 Angular

Al principio habia pensado utilizar Vue, incluso llegando a realizar un pequeño poc de la parte de front, pero pude ver que la ultima actualización de Angular llegó al nivel de Vue y React.
Además, Angular tiene una gran comunidad apoyada por Google, por lo que considero que el dia de mañana me puede traer mas oportunidades que trabajar el TFG con Vue. Angular tiene una extensa colección de librerias utiles que para mi proyecto he utilizado como por ejemplo ngx-pagination para dividir las tablas de usuarios o registros y ngx-lottie que permite la incorporación de animaciones complejas a través de JSON.

## **Crear la Maquina Docker** :whale:

Existe un archivo Dockerfile que genera una máquina con la aplicación, solamente hay que ejecutarla en Docker para crearla y poder ejecutar la aplicación.

Primero de todo tendremos que hacer un build de nuestra máquina:

```
docker build -t simulador-hardware .
```
## :wrench: Como ejecutar la aplicación 

### :whale: **Desde Docker** 


Una vez tenemos la máquina Docker, tendremos que ejecutarla con los parametros siguientes:

```
docker run --name simulador-hardware simulador-hardware <Maquina> <Cabecera> <Body> <IPDestino> <Puerto destino> <EPS> <ModoEnvio>
```

### :running: **Desde Postman** 

Una vez tenemos la máquina Docker, tendremos que ejecutarla sin parametros:

```
docker run --name simulador-hardware --network="host" simulador-hardware 
```

Una vez arrancada, iremos a Postman y nos loguearemos, para que nos devuelva un token:

![Login_Postman](./imagedocumentation/login_Postman.jpg)

_**Es importante guardar el token como Auth Token en Authorization**_

Luego haremos una petición GET a la siguiente dirección:

```
http://localhost:8080/<Maquina>
```

Y en params pondremos los siguientes valores:

| Key                | Value                   |
|--------------------|-------------------------|
| `tipoHardware`      | String - Hardware       |
| `formatoCabecera`   | String - Cabecera       |
| `formatoCuerpo`      | String - Body           |
| `direccionIPDestino` | String - IPDestino      |
| `puertoDestino`      | Integer - PuertoDestino |
| `EPS`                | Integer - EPS           |
| `modoEnvio`          | Integer - ModoEnvio     |

Una vez hecho esto, le daremos a Send y empezará a enviar los mensajes.

Aqui se muestra el resultado de una petición con los valores indicados:

![Postman](./imagedocumentation/Postman_Execution.jpg)

## :wrench: **Parámetros de la aplicación** 
Las variables indicadas serían las siguientes:

- **Maquina**: 

    Los tipos actualmente pueden ser:

  - Firewall
  - VPN
  

- **Cabecera**: 

    Es el tipo de cabecera que usaremos, en este caso podrían ser dos:

  - RFC3164
  - RFC5424


- **Body**:

    Es el tipo de cuerpo que usaremos para el mensaje, los tipos serían:

  - CEF
  - LEF
  - Key-Value
  - ApacheLog
  

- **IPDestino**:

    Es la IP donde mandaremos los mensajes UDP. Debe ser una IP válida y visible para la aplicación.


- **PuertoDestino**:

    Es el puerto de la máquina de destino, en caso del servidor Rsyslog, el más común es el 514.


- **EPS**: 

    Es la tasa de eventos por segundo, índica cuantos mensajes enviaremos a la máquina por segundo. ¡Importante no confundir con el tiempo entre mensajes! Si indicamos un EPS de 2, enviaremos un mensaje cada 0.5 segundos.


- **ModoEnvio**:

    Es el tipo de envio que queremos, podemos hacerlo de manera limitada poniendo directamente el número de mensajes que queremos enviar o indicando -1 para que se envie de manera continua e ilimitada.

    Por ejemplo, si ponemos 10, nos enviará 10 mensajes y finalizará la ejecución.

    Es importante tener en cuenta que actualmente el -1 está implementado para pruebas en local ya que en la máquina AWS no se realizó el script de parada de java con kill. es por ello que en local si que seria posible utilizar el -1 pero en AWS **NO**.  
 
## 💻 Máquina Servidor Rsyslog 

Para poder recibir los mensajes, necesitamos un servidor Rsyslog, para ello he creado un dockerfile para que sea más visible el resultado de la aplicación.
Tendremos que movernos al siguiente directorio estando en la raiz del proyecto:

```
cd Rsyslog
```

Una vez estemos en la carpeta correspondiente, podemos hacer el build con el siguiente comando:

```
docker build -f Dockerfile -t rsyslog-server . 
```

Y después ejecutarla con el siguiente comando:

```
docker run -p 0.0.0.0:0:514 --name rsyslog-Server --network="host" rsyslog-server 
```

Con esto, tendremos un servidor Rsyslog escuchando en el puerto 514, y podremos recibir los mensajes que enviemos con la aplicación.

Al ejecutarla lo que nos aparecerá será lo siguiente:

![Rsyslog](./imagedocumentation/RsyslogStart.jpg)

## 🧪 **Testing completo de la aplicación** 

Para poder hacer un testing completo de la aplicación se creó un docker-compose.yml en el cual tendremos las máquinas con la configuración necesaria para ejecutarlas. Tendremos que seguir los siguientes pasos:

  - Ejecutamos el siguiente comando en la raiz para poder crear las máquinas:
    
      ```
      docker-compose build
      ```
  
  - Una vez creadas, ejecutamos el siguiente comando para poder arrancarlas:

      ```
      docker-compose up -d
      ```
  
Esto nos creará las máquinas y nos dejará escuchando el SpringBoot y Rsyslog. Es muy importante fijarse en la máquina de Rsyslog ya que nos indica la IP de la máquina a la cual le tenemos que indicar en el parametro 'direccionIPDestino' para que le lleguen los mensajes.

## 👤 Usuarios

| Tipo | Descripcion|
| :----: |------------|
| Administrador | Es el usuario que podrá crear, eliminar y visualizar usuarios. Tiene control total de la aplicación ya que puede entrar en el resto de paneles. |
| Mantainer | Usuario que puede visualizar las ejecuciones realizadas por los usuarios. Además puede lanzar ejecuciones. |
| User | Usuario que solamente puede realizar ejecuciones. |

Usuarios de prueba para la aplicación desde AWS. (En caso de utilizar el local, la maquina docker instanciaria solamente el usuario admin.)

| usuario | contraseña |
| :----: | :----: |
| admin |MyP@$$|
| mantainer | mantainer |
| user | user |

## 💻 Pantallas

A continuación se describen las pantallas y que usuarios pueden acceder

| Pantalla | Descripcion | Admin | Mantainer | User |
| :----: |--------| :----: | :----: | :----: |
| `/login` | Login de la aplicación apra obtener el token | :white_check_mark: | :white_check_mark:| :white_check_mark:|
| `/user-panel` | Panel basico de usuario donde se lanzan las ejecuciones | :white_check_mark: | :white_check_mark:| :white_check_mark:|
| `/mantainer-panel` | Panel donde se muestran los registros de ejecuciones por parte de los usuarios | :white_check_mark: | :white_check_mark:| :x:|
| `/admin-panel` | Panel donde se muestran, crean y eliminan los usuarios de la aplicación junto con sus roles | :white_check_mark: | :x:| :x:|


## 🚀 Front con Angular 17

Para este proyecto he utilizado Angular 17, ya que es una de las tecnologías más populares para el desarrollo de aplicaciones web modernas. Angular es un framework de código abierto desarrollado por Google que permite crear aplicaciones web de forma rápida y eficiente.
Además con su ultima versión, Angular 17, se han añadido nuevas características y mejoras que hacen que el desarrollo de aplicaciones sea aún más sencillo para personas como yo, que no tienen mucha experiencia en el desarrollo front-end.


### 🧩 Componentización

La arquitectura del proyecto sigue un enfoque modular, donde el código está organizado en diferentes bloques para mantener la claridad y la reutilización. Los componentes en el directorio **Pages** representan las vistas completas de la aplicación. Cada página se corresponde con una ruta específica.

#### 🏠 MainComponent

Es la vista principal de la aplicación y da acceso a través de un formulario de login. Este componente se encarga de la autenticación del usuario y de la redirección a la página de inicio dependiendo del rol del usuario.

#### 👨‍💼 AdminPanelComponent

Esta vista proporciona una interfaz para administrar los usuarios de la aplicación. Permite ver la lista de usuarios, eliminar usuarios y agregar nuevos usuarios. Solo es accesible para usuarios con rol "admin".

#### 👥 UserPanelComponent

La vista principal de la aplicación para los usuarios. Nos da acceso a la funcionalidad principal de la aplicación.

#### 🛠️ MaintainerPanelComponent

La vista de mantenimiento nos da acceso a una lista de todos los registros de la aplicación, permitiendo una vista detallada de cada uno de ellos y paginados para una mejor visualización.

#### 🗂️ PanelMenuComponent

Finalmente, este pequeño componente nos aporta la navegabilidad entre las diferentes vistas de la aplicación, teniendo dos entradas: una que determina qué tipo de usuario está utilizando la aplicación y otra que nos permite saber en qué vista nos encontramos. Con esto podemos tener un control de la navegabilidad y de la información que se muestra en la aplicación, de una forma más sencilla y clara, permitiendo de manera más eficiente la navegación entre las diferentes vistas de la aplicación dependiendo del tipo de usuario que la esté utilizando.

### 🔧 Servicios

En este proyecto se utilizan varios servicios que proporcionan funcionalidades compartidas entre diferentes componentes de la aplicación.

#### 🔐 JwtDecoderService

Este servicio, `JWTDecoderService`, se encarga de decodificar los tokens JWT (JSON Web Tokens). Es esencial para interpretar la información contenida en los tokens, que se utilizan para la autenticación y la autorización en la aplicación.

#### 🕒 SessionService

El `SessionService` maneja la sesión del usuario utilizando el almacenamiento de sesión del navegador. Permite almacenar, recuperar y eliminar datos de sesión, manteniendo el estado del usuario durante toda su interacción en la aplicación.

#### 🌐 ApiService

El `ApiService` realiza las solicitudes HTTP a la API del servidor. Se encarga de la comunicación con el servidor, enviando y recibiendo datos a través de solicitudes HTTP. Proporciona métodos para realizar solicitudes GET, POST y DELETE, y maneja los errores de las solicitudes.

### 📄 Interfaces

Las interfaces se utilizan para definir la estructura de los datos y asegurar que los objetos utilizados sigan un tipado estricto, lo cual es ventajoso especialmente a la hora de programar con TypeScript.

### 🌍 Enrutado

El enrutado en Angular facilita la navegación entre las diferentes vistas de la aplicación. Se define en el archivo `app-routing.module.ts` y se configura con las rutas de la aplicación.

### 🚨 Guards

Los guards son servicios que se utilizan para proteger las rutas de la aplicación. Se utilizan para restringir el acceso a ciertas rutas dependiendo del rol de cada usuario.

### Como compilar Angular

Para compilar el Angular, es necesario tener instalado Node.
Después tendremos que instalar Angular con el siguiente comando:

```
npm install -g @angular/cli
```

Una vez instalado, accedemos a la carpeta del angular, donde se encuentra nuestro angular.json y ejecutamos el comando de instalación de paquetes:

```
npm install
```

Ahora ejecutamos el comando
```
ng build
```

Con esto habremos conseguido compilar la carpeta de angular en otra llamada 'dist'. Se generará una carpeta llamada browser que contendrá todo el codigo angular compilado en archivos js.
Copiaremos el contenido de la carpeta y lo pondremos en nuestro servidor apache para ejecutarlo.

## Endpoints

| Endopoint | Tipo | Descripción | Parametros | Body | Resultado | Rol minimo |
| :----: | :----: | ---- | ---- | ---- | ----| ----|
| `/ping` | `GET` | Es el heartbeat, se utiliza para comprobar la conectividad con el servidor. | **NONE** | **NONE** | String: 'Pong' | **NONE** |
| `/auth/login` | `POST` | Se utiliza para obtener el token en caso de login correcto |  **NONE**  | "username":"{Usuario}", "password":"{Contraseña}" | String token | **NONE** |
| `/auth/register` | `POST` | Registro de usuarios | **NONE** | "name":"{Nombre}", "username":"{Usuario}", "password":"{Contraseña}", "role": "{Rol}" | String: 'Usuario registrado correctamente' | `ADMIN` |
| `/auth/{id}` | `DELETE` | Elimina el usuario con el id indicado en la llamada | **NONE** | **NONE** | String: 'Usuario eliminado exitosamente' | `ADMIN` |
| `/auth/all` | `GET` | Muestra todos los usuarios en formato lista |**NONE** | **NONE** | Lista de String con los usuarios | `ADMIN` |
| `/registries` | `GET` | Muestra todas las ejecuciones en formato lista | **NONE** | **NONE** | Lista de String con las ejecuciones | `MANTAINER` |
| `/firewall` | `GET` | Lanza una ejecución hacia el servidor indicado | ?tipoHardware={TipoHardware}&formatoCabecera={FormatoCabecera}&formatoCuerpo={FormatoCuerpo}&direccionIPDestino={IPDestino}&puertoDestino={PuertoDestino}&EPS={EPS}&modoEnvio={ModoEnvio} | **NONE** | String: 'Ejecución correcta.' | `USER` |
