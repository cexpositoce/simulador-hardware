FROM maven:3.8.4-openjdk-17-slim AS build
LABEL authors="Carlos Exposito"
WORKDIR /app

COPY pom.xml .
RUN mvn dependency:go-offline

COPY src src
RUN mvn clean package -DskipTests

FROM eclipse-temurin:17-jre-alpine
LABEL authors="Carlos Exposito"
WORKDIR /app

COPY --from=build /app/target/simulador-hardware-1.0.jar ./app.jar
EXPOSE 8080:8080

ENTRYPOINT ["java", "-jar", "app.jar"]