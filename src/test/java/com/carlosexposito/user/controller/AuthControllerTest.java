package com.carlosexposito.user.controller;
import com.carlosexposito.security.JwtUtil;
import com.carlosexposito.user.controller.exceptions.UserNotFoundException;
import com.carlosexposito.user.model.User;
import com.carlosexposito.user.model.UserRepository;
import com.carlosexposito.user.service.UserDetailsServiceImpl;
import com.carlosexposito.user.service.request.AuthRequest;
import com.carlosexposito.user.service.request.CreateUserRequest;
import com.carlosexposito.user.service.response.UserResponse;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AuthControllerTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtUtil jwtUtil;

    @Mock
    private UserDetailsServiceImpl userDetailsService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AuthController authController;

    private Faker faker;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        faker = new Faker();
    }

    @Test
    void testLoginSuccess() {
        String username = faker.name().username();
        String password = faker.internet().password();
        User user = new User();
        user.setId(1L);
        user.setUsername(username);
        user.setPassword(password);
        user.setRole("User");

        AuthRequest authRequest = new AuthRequest(username, password);

        UserDetails userDetails = mock(UserDetails.class);
        when(userDetails.getUsername()).thenReturn(username);

        when(userRepository.findByUsername(authRequest.username())).thenReturn(Optional.of(user));
        when(jwtUtil.generateToken(username,"User")).thenReturn("fake-jwt-token");

        ResponseEntity<String> response = authController.login(authRequest);

        assertEquals(200, response.getStatusCode().value());
        assertEquals("fake-jwt-token", response.getBody());
    }

    @Test
    void testLoginFailure() {
        String username = faker.name().username();
        String password = faker.internet().password();
        AuthRequest authRequest = new AuthRequest(username, password);

        doThrow(BadCredentialsException.class).when(authenticationManager).authenticate(any(UsernamePasswordAuthenticationToken.class));

        ResponseEntity<String> response = authController.login(authRequest);

        assertEquals(401, response.getStatusCode().value());
        assertEquals("Credenciales inválidas", response.getBody());
    }

    @Test
    void testRegisterUserAlreadyExists() {
        String username = faker.name().username();
        CreateUserRequest createUserRequest = new CreateUserRequest(faker.name().fullName(), username, faker.internet().password(), "User");

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(new User()));

        ResponseEntity<String> response = authController.register(createUserRequest);

        assertEquals(400, response.getStatusCode().value());
        assertEquals("El usuario ya existe", response.getBody());
    }

    @Test
    void testRegisterSuccess() {
        String username = faker.name().username();
        CreateUserRequest createUserRequest = new CreateUserRequest(faker.name().fullName(), username, faker.internet().password(), "User");

        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());
        when(passwordEncoder.encode(anyString())).thenReturn("encoded-password");

        ResponseEntity<String> response = authController.register(createUserRequest);

        assertEquals(200, response.getStatusCode().value());
        assertEquals("Usuario registrado correctamente", response.getBody());
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void testDeleteUserNotFound() {
        Long userId = faker.number().randomNumber();

        when(userRepository.existsById(userId)).thenReturn(false);

        ResponseEntity<String> response = authController.deleteUser(userId);

        assertEquals(404, response.getStatusCode().value());
        assertEquals("Usuario no encontrado", response.getBody());
    }

    @Test
    void testDeleteUserSuccess() {
        Long userId = faker.number().randomNumber();

        when(userRepository.existsById(userId)).thenReturn(true);

        ResponseEntity<String> response = authController.deleteUser(userId);

        assertEquals(200, response.getStatusCode().value());
        assertEquals("Usuario eliminado exitosamente", response.getBody());
        verify(userRepository, times(1)).deleteById(userId);
    }

    @Test
    void testFindUserNotFound() {
        Long userId = faker.number().randomNumber();

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> authController.findUser(userId));
    }

    @Test
    void testFindUserSuccess() {
        Long userId = faker.number().randomNumber();
        User user = new User();
        user.setId(userId);
        user.setUsername(faker.name().username());
        user.setRole("User");

        when(userRepository.existsById(userId)).thenReturn(true);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        UserResponse response = authController.findUser(userId);

        assertNotNull(response);
        assertEquals(userId.toString(), response.id());
        assertEquals(user.getUsername(), response.username());
        assertEquals(user.getRole(), response.role());
    }

    @Test
    void testGetListUserSuccess() {
        Long userId = faker.number().randomNumber();
        User user = new User();
        user.setId(userId);
        user.setUsername(faker.name().username());
        user.setRole("User");
        List<User> users = List.of(user);

        when(userRepository.findAll()).thenReturn(users);

        List<UserResponse> response = authController.getAll();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(userId.toString(), response.get(0).id());
        assertEquals(user.getUsername(), response.get(0).username());
        assertEquals(user.getRole(), response.get(0).role());
    }

}