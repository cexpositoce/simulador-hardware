package com.carlosexposito.user.model;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = User.class)
class UserIntegrationTest {


    private Faker faker;

    @BeforeEach
    void setUp() {
        faker = new Faker();
    }

    @Test
    void testCreateUser() {
        User user = new User();
        user.setId(faker.number().randomNumber());
        user.setName(faker.name().fullName());
        user.setUsername(faker.name().username());
        user.setPassword(faker.internet().password());
        user.setRole("User");

        assertNotNull(user.getId());
        assertNotNull(user.getName());
        assertNotNull(user.getUsername());
        assertNotNull(user.getPassword());
        assertNotNull(user.getRole());
    }

}