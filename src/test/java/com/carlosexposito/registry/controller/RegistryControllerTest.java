package com.carlosexposito.registry.controller;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import com.carlosexposito.registry.model.Registry;
import com.carlosexposito.registry.model.RegistryRepository;
import com.carlosexposito.registry.service.response.RegistryResponse;
import com.carlosexposito.user.model.User;
import com.carlosexposito.user.model.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class RegistryControllerTest {

    @Mock
    private RegistryRepository registryRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private RegistryController registryController;

    private List<Registry> registryList;

    @BeforeEach
    void setUp() {
        registryList = new ArrayList<>();
        Registry registry1 = new Registry();
        registry1.setId(1L);
        registry1.setHardware("hardware1");
        registry1.setHeaderType("headerType1");
        registry1.setBody("body1");
        registry1.setIp("127.0.0.1");
        registry1.setPort("8080");
        registry1.setEps("10");
        registry1.setIdUser(1L);
        registry1.setSending("1000");
        Registry registry2 = new Registry();
        registry2.setId(2L);
        registry2.setHardware("hardware2");
        registry2.setHeaderType("headerType2");
        registry2.setBody("body2");
        registry1.setIp("127.0.0.1");
        registry1.setPort("8080");
        registry1.setEps("10");
        registry2.setIdUser(2L);
        registry2.setSending("1000");
        registryList.add(registry1);
        registryList.add(registry2);
    }

    @Test
    void testGetAllWithExistingUsers() {
        when(registryRepository.findAll()).thenReturn(registryList);

        User user1 = new User();
        user1.setId(1L);
        user1.setName("user1");
        user1.setUsername("user1");
        user1.setPassword("password1");
        user1.setRole("ROLE_USER");
        User user2 = new User();
        user2.setId(2L);
        user2.setName("user2");
        user2.setUsername("user2");
        user2.setPassword("password2");
        user2.setRole("ROLE_USER");
        when(userRepository.existsById(1L)).thenReturn(true);
        when(userRepository.existsById(2L)).thenReturn(true);
        when(userRepository.findById(1L)).thenReturn(Optional.of(user1));
        when(userRepository.findById(2L)).thenReturn(Optional.of(user2));

        List<RegistryResponse> result = registryController.getAll();

        assertEquals(2, result.size());
        assertEquals("user1", result.get(0).user().username());
        assertEquals("user2", result.get(1).user().username());
    }

    @Test
    void testGetAllWithNonExistingUser() {
        when(registryRepository.findAll()).thenReturn(registryList);

        User user1 = new User();
        user1.setId(1L);
        user1.setName("user1");
        user1.setUsername("user1");
        user1.setPassword("password1");
        user1.setRole("ROLE_USER");
        when(userRepository.existsById(1L)).thenReturn(true);
        when(userRepository.existsById(2L)).thenReturn(false);
        when(userRepository.findById(1L)).thenReturn(Optional.of(user1));

        List<RegistryResponse> result = registryController.getAll();

        assertEquals(2, result.size());
        assertEquals("user1", result.get(0).user().username());
        assertEquals("Deleted", result.get(1).user().username());
    }
}
