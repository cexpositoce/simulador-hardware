package com.carlosexposito.security;


import com.carlosexposito.user.model.User;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.Date;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = User.class)
@TestPropertySource(properties = {
        "jwt.secret=testsecret",
        "jwt.token.validity=3600000"
})
class JwtUtilIntegrationTest {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.token.validity}")
    private long tokenValidity;

    private JwtUtil jwtUtil;

    @BeforeEach
    void setUp() {
        jwtUtil = new JwtUtil();
        jwtUtil.secret = secret;
        jwtUtil.tokenValidity = tokenValidity;
    }

    @Test
    void testGenerateToken() {
        String username = "testuser";
        String token = jwtUtil.generateToken(username, "User");

        assertNotNull(token);
        assertTrue(token.length() > 0);

        String extractedUsername = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        assertEquals(username, extractedUsername);
    }

    @Test
    void testExtractUsername() {
        String username = "testuser";
        String token = jwtUtil.generateToken(username, "User");

        String extractedUsername = jwtUtil.extractUsername(token);

        assertEquals(username, extractedUsername);
    }

    @Test
    void testExtractExpiration() {
        String username = "testuser";
        String token = jwtUtil.generateToken(username, "User");

        Date expiration = jwtUtil.extractExpiration(token);

        assertNotNull(expiration);
        assertTrue(expiration.after(new Date()));
    }

    @Test
    void testValidateToken() {
        String username = "testuser";
        String token = jwtUtil.generateToken(username, "User");

        Boolean isValid = jwtUtil.validateToken(token, username);

        assertTrue(isValid);
    }

    @Test
    void testIsTokenExpired() {
        String username = "testuser";
        String token = Jwts.builder()
                .setSubject(username)
                .addClaims(Map.of("role", "User"))
                .setIssuedAt(new Date(System.currentTimeMillis() - 1000000000))
                .setExpiration(new Date(System.currentTimeMillis() - 1000000000)) // Token is already expired
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();

        Boolean isExpired = jwtUtil.validateToken(token, username);

        assertFalse(isExpired);
    }

    @Test
    void testValidateToken_ValidToken_ValidUser() {
        String token = "validToken";
        String username = "testuser";

        JwtUtil jwtUtilSpy = Mockito.spy(jwtUtil);
        Mockito.doReturn(username).when(jwtUtilSpy).extractUsername(token);
        Mockito.doReturn(false).when(jwtUtilSpy).isTokenExpired(token);

        Boolean isValid = jwtUtilSpy.validateToken(token, username);

        assertTrue(isValid);
    }

    @Test
    void testValidateToken_ValidToken_InvalidUser() {
        String token = "validToken";
        String username = "testuser";

        JwtUtil jwtUtilSpy = Mockito.spy(jwtUtil);
        Mockito.doReturn("differentuser").when(jwtUtilSpy).extractUsername(token);
        Mockito.doReturn(false).when(jwtUtilSpy).isTokenExpired(token);

        Boolean isValid = jwtUtilSpy.validateToken(token, username);

        assertFalse(isValid);
    }

    @Test
    void testValidateToken_ExpiredToken_ValidUser() {
        String token = "validToken";
        String username = "testuser";

        JwtUtil jwtUtilSpy = Mockito.spy(jwtUtil);
        Mockito.doReturn(username).when(jwtUtilSpy).extractUsername(token);
        Mockito.doReturn(true).when(jwtUtilSpy).isTokenExpired(token);

        Boolean isValid = jwtUtilSpy.validateToken(token, username);

        assertFalse(isValid);
    }

    @Test
    void testValidateToken_ExpiredToken_InvalidUser() {
        String token = "validToken";
        String username = "testuser";

        JwtUtil jwtUtilSpy = Mockito.spy(jwtUtil);
        Mockito.doReturn("differentuser").when(jwtUtilSpy).extractUsername(token);
        Mockito.doReturn(true).when(jwtUtilSpy).isTokenExpired(token);

        Boolean isValid = jwtUtilSpy.validateToken(token, username);

        assertFalse(isValid);
    }

    @Test
    void testValidateToken_ExpiredJwtException() {
        String token = "expiredToken";
        String username = "testuser";

        JwtUtil jwtUtilSpy = Mockito.spy(jwtUtil);
        Mockito.doThrow(ExpiredJwtException.class).when(jwtUtilSpy).extractUsername(token);

        Boolean isValid = jwtUtilSpy.validateToken(token, username);

        assertFalse(isValid);
    }
}