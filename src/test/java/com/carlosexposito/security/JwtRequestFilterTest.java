package com.carlosexposito.security;

import com.carlosexposito.user.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class JwtRequestFilterTest {

    @Mock
    private JwtUtil jwtUtil;

    @Mock
    private UserDetailsServiceImpl userDetailsService;

    @InjectMocks
    private JwtRequestFilter jwtRequestFilter;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Mock
    private UserDetails userDetails;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        SecurityContextHolder.clearContext();
    }

    @Test
    void testDoFilterInternalWithValidToken() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn("Bearer validToken");
        when(jwtUtil.extractUsername("validToken")).thenReturn("username");
        when(userDetailsService.loadUserByUsername("username")).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn("username");
        when(jwtUtil.validateToken("validToken", "username")).thenReturn(true);

        jwtRequestFilter.doFilterInternal(request, response, filterChain);

        verify(request, times(1)).getHeader("Authorization");
        verify(jwtUtil, times(1)).extractUsername("validToken");
        verify(userDetailsService, times(1)).loadUserByUsername("username");
        verify(jwtUtil, times(1)).validateToken("validToken", "username");
        verify(filterChain, times(1)).doFilter(request, response);

        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authenticationToken);
        assertNotNull(authenticationToken.getPrincipal());
        assertEquals(userDetails, authenticationToken.getPrincipal());
    }

    @Test
    void testDoFilterInternalWithInvalidToken() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn("Bearer invalidToken");
        when(jwtUtil.extractUsername("invalidToken")).thenReturn("username");
        when(userDetailsService.loadUserByUsername("username")).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn("username");
        when(jwtUtil.validateToken("invalidToken", "username")).thenReturn(false);

        jwtRequestFilter.doFilterInternal(request, response, filterChain);

        verify(request, times(1)).getHeader("Authorization");
        verify(jwtUtil, times(1)).extractUsername("invalidToken");
        verify(userDetailsService, times(1)).loadUserByUsername("username");
        verify(jwtUtil, times(1)).validateToken("invalidToken", "username");
        verify(filterChain, times(1)).doFilter(request, response);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    void testDoFilterInternalWithNoToken() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn(null);

        jwtRequestFilter.doFilterInternal(request, response, filterChain);

        verify(request, times(1)).getHeader("Authorization");
        verify(jwtUtil, times(0)).extractUsername(anyString());
        verify(userDetailsService, times(0)).loadUserByUsername(anyString());
        verify(jwtUtil, times(0)).validateToken(anyString(), anyString());
        verify(filterChain, times(1)).doFilter(request, response);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    void testDoFilterInternalWithTokenNotStartingWithBearer() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn("Basic token");

        jwtRequestFilter.doFilterInternal(request, response, filterChain);

        verify(request, times(1)).getHeader("Authorization");
        verify(jwtUtil, times(0)).extractUsername(anyString());
        verify(userDetailsService, times(0)).loadUserByUsername(anyString());
        verify(jwtUtil, times(0)).validateToken(anyString(), anyString());
        verify(filterChain, times(1)).doFilter(request, response);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    void testDoFilterInternalWithAuthenticatedUser() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn("Bearer validToken");
        when(jwtUtil.extractUsername("validToken")).thenReturn("username");
        when(userDetailsService.loadUserByUsername("username")).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn("username");
        when(jwtUtil.validateToken("validToken", "username")).thenReturn(true);

        // Set an existing authentication in the security context
        UsernamePasswordAuthenticationToken existingAuth = new UsernamePasswordAuthenticationToken("existingUser", null, null);
        SecurityContextHolder.getContext().setAuthentication(existingAuth);

        jwtRequestFilter.doFilterInternal(request, response, filterChain);

        verify(request, times(1)).getHeader("Authorization");
        verify(jwtUtil, times(1)).extractUsername("validToken");
        verify(userDetailsService, times(0)).loadUserByUsername("username");
        verify(jwtUtil, times(0)).validateToken("validToken", "username");
        verify(filterChain, times(1)).doFilter(request, response);

        // The authentication should not change because there was already an authentication
        assertEquals(existingAuth, SecurityContextHolder.getContext().getAuthentication());
    }
}