package com.carlosexposito.config;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HardwareConfigTest {

    @Test
    void settersTesting() {
        HardwareConfig hc = new HardwareConfig("Firewall", "formatoCabecera", "formatoCuerpo",
                "direccionIPDestino", 8080, 10, 1);
        hc.setTipoHardware("Firewall");
        hc.setFormatoCabecera("formatoCabecera");
        hc.setFormatoCuerpo("formatoCuerpo");
        hc.setDireccionIPDestino("direccionIPDestino");
        hc.setPuertoDestino(8080);
        hc.setEPS(10);
        hc.setModoEnvio(1);

        assertEquals("Firewall", hc.getTipoHardware());
        assertEquals("formatoCabecera", hc.getFormatoCabecera());
        assertEquals("formatoCuerpo", hc.getFormatoCuerpo());
        assertEquals("direccionIPDestino", hc.getDireccionIPDestino());
        assertEquals(8080, hc.getPuertoDestino());
        assertEquals(10, hc.getEPS());
        assertEquals(1, hc.getModoEnvio());
    }

}