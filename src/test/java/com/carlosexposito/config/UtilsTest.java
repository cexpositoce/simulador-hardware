package com.carlosexposito.config;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilsTest {


    @Test
    void ping() {
        Utils utils = new Utils();
        assertEquals("pong", utils.ping());
    }
}