package com.carlosexposito.hardware;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.firewall.model.Firewall;
import com.carlosexposito.hardware.message.MessageSenderService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HardwareTest {

    @Test
    void hardwareSetters() {
        HardwareConfig hc = new HardwareConfig("Firewall", "formatoCabecera", "formatoCuerpo",
                "direccionIPDestino", 8080, 10, 1);
        MessageSenderService mss = new MessageSenderService();
        Firewall firewall = new Firewall(mss,hc);
        firewall.setFormatoCabecera("formatoCabecera");
        firewall.setFormatoCuerpo("formatoCuerpo");
        firewall.setDireccionIPDestino("direccionIPDestino");
        firewall.setPuertoDestino(8080);
        firewall.setEPS(10);
        firewall.setModoEnvio(1);
        MessageSenderService message = new MessageSenderService();
        firewall.setMessageService(message);

        assertEquals("formatoCabecera", firewall.getFormatoCabecera());
        assertEquals("formatoCuerpo", firewall.getFormatoCuerpo());
        assertEquals("direccionIPDestino", firewall.getDireccionIPDestino());
        assertEquals(8080, firewall.getPuertoDestino());
        assertEquals(10, firewall.getEPS());
        assertEquals(1, firewall.getModoEnvio());
        assertEquals(message, firewall.getMessageService());
    }

    @Test
    void messageSenderNull() {
        HardwareConfig hc = new HardwareConfig("Firewall", "formatoCabecera", "formatoCuerpo",
                "direccionIPDestino", 8080, 10, 1);
        MessageSenderService mss = new MessageSenderService();
        Firewall firewall = new Firewall(mss,hc);

        firewall.setMessageService(null);
        firewall.sendMessage("message");

        assertEquals(null, firewall.getMessageService());
    }

}