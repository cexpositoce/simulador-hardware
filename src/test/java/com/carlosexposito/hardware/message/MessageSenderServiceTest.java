package com.carlosexposito.hardware.message;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.event.Level;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static com.carlosexposito.config.Constants.MENSAJE_ENVIADO_ERROR;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class MessageSenderServiceTest {
    private TestLogger testLogger;

    @Mock
    private DatagramSocket socketMock;

    @InjectMocks
    private MessageSenderService messageSenderService;

    @Captor
    ArgumentCaptor<DatagramPacket> packetCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        testLogger = TestLoggerFactory.getTestLogger(MessageSenderService.class);
        testLogger.clear();
    }

    @AfterEach
    void tearDown() {
        TestLoggerFactory.clear();
    }

    @Test
    void enviaMensajeCorrecto() {
        MessageSenderService sender = new MessageSenderService();
        assertThatCode(() -> sender.sendMessage("127.0.0.1", 8080, "Test message")).doesNotThrowAnyException();
    }

    @Test
    void testSendMessageWithException() throws Exception {
        String direccionIPDestino = "127.0.0.1";
        int puertoDestino = 8080;
        String message = "Test Message";

        // Mock InetAddress
        InetAddress inetAddressMock = mock(InetAddress.class);

        // Spy on DatagramSocket and mock its methods
        try (MockedStatic<InetAddress> mockedInetAddress = mockStatic(InetAddress.class)) {
            mockedInetAddress.when(() -> InetAddress.getByName(direccionIPDestino)).thenReturn(inetAddressMock);

            // Spy on the MessageSenderService to mock createDatagramSocket
            MessageSenderService spyService = spy(messageSenderService);
            doReturn(socketMock).when(spyService).createDatagramSocket();
            doThrow(new RuntimeException("Test Exception")).when(socketMock).send(any(DatagramPacket.class));

            // Execute the method
            spyService.sendMessage(direccionIPDestino, puertoDestino, message);

            // Verify that the error was logged
            boolean logFound = testLogger.getLoggingEvents().stream()
                    .anyMatch(event -> event.getLevel().equals(Level.ERROR) &&
                            event.getMessage().equals(MENSAJE_ENVIADO_ERROR + "{}:{} - {}") &&
                            event.getArguments().get(0).equals(direccionIPDestino) &&
                            event.getArguments().get(1).equals(puertoDestino) &&
                            event.getArguments().get(2).equals("Test Exception"));

            assertFalse(logFound, "Expected log message not found");
        }
    }


}