package com.carlosexposito.hardware.firewall.model;

import com.carlosexposito.config.HeaderFormat;
import com.carlosexposito.config.BodyType;
import com.carlosexposito.hardware.message.LogGenerator;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.*;

class FirewallLogGeneratorTest {

    @Test
    void testPrivateConstructor() {
        Constructor<LogGenerator> constructor = null;
        try {
            constructor = LogGenerator.class.getDeclaredConstructor();
            constructor.setAccessible(true);
        } catch (NoSuchMethodException e) {
            fail("Constructor not found: " + e);
        }

        try {
            if (constructor != null) {
                constructor.newInstance();
                fail("Expected IllegalStateException to be thrown");
            }
        } catch (InstantiationException | IllegalAccessException e) {
            fail("Unexpected exception: " + e);
        } catch (InvocationTargetException e) {
            assertTrue(e.getCause() instanceof IllegalStateException);
            assertEquals("Utility class", e.getCause().getMessage());
        }
    }

    @Test
    void testGenerateRFC3164CEF() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_3164, BodyType.CEF, 1);
        assertTrue(log.contains("CEF"));
        assertTrue(log.contains("Log message number 1"));
    }

    @Test
    void testGenerateRFC3164LEF() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_3164, BodyType.LEF, 1);
        assertTrue(log.contains("LEF"));
        assertTrue(log.contains("Log message number 1"));
    }

    @Test
    void testGenerateRFC3164KeyValue() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_3164, BodyType.KEY_VALUE, 1);
        assertTrue(log.contains("message=Log message number 1"));
        assertTrue(log.contains("sourceIP="));
        assertTrue(log.contains("destinationIP="));
    }

    @Test
    void testGenerateRFC3164ApacheLog() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_3164, BodyType.APACHE_LOG, 1);
        assertTrue(log.contains("GET / HTTP/1.1"));
        assertTrue(log.contains(" 200 100"));
    }

    @Test
    void testGenerateRFC5424CEF() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_5424, BodyType.CEF, 1);
        assertTrue(log.contains("CEF"));
        assertTrue(log.contains("Log message number 1"));
    }

    @Test
    void testGenerateRFC5424LEF() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_5424, BodyType.LEF, 1);
        assertTrue(log.contains("LEF"));
        assertTrue(log.contains("Log message number 1"));
    }

    @Test
    void testGenerateRFC5424KeyValue() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_5424, BodyType.KEY_VALUE, 1);
        assertTrue(log.contains("message=Log message number 1"));
        assertTrue(log.contains("sourceIP="));
        assertTrue(log.contains("destinationIP="));
    }

    @Test
    void testGenerateRFC5424ApacheLog() {
        String log = LogGenerator.generateLog("Firewall",HeaderFormat.RFC_5424, BodyType.APACHE_LOG, 1);
        assertTrue(log.contains("GET / HTTP/1.1"));
        assertTrue(log.contains(" 200 100"));
    }
    @Test
    void testGenerateFail(){
        String log = LogGenerator.generateLog("Firewall",null, null, 1);
        assertFalse(log.contains("CEF"));
        assertFalse(log.contains("Log message number 1"));
    }
}
