package com.carlosexposito.hardware.firewall.model;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.message.MessageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

class FirewallTest {

    @Mock
    private MessageService messageService;

    private Firewall firewall;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        HardwareConfig hc = new HardwareConfig("Firewall", "headerFormat", "bodyFormat", "127.0.0.1", 8080, 10, 1);
        firewall = new Firewall(messageService, hc);
    }

    @Test
    void enviaMensajeCorrecto() {
        String testMessage = "Test message";
        firewall.sendMessage(testMessage);

        verify(messageService).sendMessage("127.0.0.1", 8080, testMessage);
    }
}