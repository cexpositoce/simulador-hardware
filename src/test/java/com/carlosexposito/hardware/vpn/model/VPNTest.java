package com.carlosexposito.hardware.vpn.model;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.message.MessageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

class VPNTest {

    @Mock
    private MessageService messageService;

    private VPN vpn;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        HardwareConfig hc = new HardwareConfig("Firewall", "headerFormat", "bodyFormat", "127.0.0.1", 8080, 10, 1);
        vpn = new VPN(messageService, hc);
    }

    @Test
    void enviaMensajeCorrecto() {
        String testMessage = "Test message";
        vpn.sendMessage(testMessage);

        verify(messageService).sendMessage("127.0.0.1", 8080, testMessage);
    }
}