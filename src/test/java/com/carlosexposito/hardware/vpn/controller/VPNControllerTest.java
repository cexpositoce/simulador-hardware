package com.carlosexposito.hardware.vpn.controller;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.vpn.service.VPNService;
import com.carlosexposito.registry.model.RegistryRepository;
import com.carlosexposito.user.model.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class VPNControllerTest {

    @Mock
    private VPNService vpnService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RegistryRepository registryRepository;

    @InjectMocks
    private VPNController vpnController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(vpnController).build();
    }

    @Test
    void devuelveLlamadaCorrecta() throws Exception {
        try (MockedStatic<SecurityContextHolder> mocked = Mockito.mockStatic(SecurityContextHolder.class)) {
            SecurityContext securityContextMock = mock(SecurityContext.class);
            Authentication authentication = mock(Authentication.class);

            when(authentication.getPrincipal()).thenReturn(new User("admin", "admin", Collections.emptyList()));
            when(securityContextMock.getAuthentication()).thenReturn(authentication);
            mocked.when(SecurityContextHolder::getContext).thenReturn(securityContextMock);
            com.carlosexposito.user.model.User user = new com.carlosexposito.user.model.User();
            user.setId(1L);
            user.setUsername("admin");
            user.setName("admin");
            user.setPassword("admin");
            user.setRole("Admin");
            when(userRepository.findByUsername(any())).thenReturn(java.util.Optional.of(user));


            mockMvc.perform(get("/VPN")
                            .param("formatoCabecera", "header")
                            .param("formatoCuerpo", "body")
                            .param("direccionIPDestino", "127.0.0.1")
                            .param("puertoDestino", "8080")
                            .param("EPS", "10")
                            .param("modoEnvio", "1")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().string("Log de VPN generado correctamente."));

            verify(vpnService, times(1)).processVPN(any(HardwareConfig.class));
        }
    }
}