package com.carlosexposito.hardware.vpn.service;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.message.MessageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class VPNServiceTest {

    @Mock
    private MessageService messageService;

    @InjectMocks
    private VPNService vpnService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void enviaMensajeCorrecto() {
        HardwareConfig hc = new HardwareConfig("VPN", "RFC_5424", "CEF",
                "127.0.0.1", 8080, 10, 1);
        vpnService.processVPN(hc);

        verify(messageService, times(1)).sendMessage(anyString(), anyInt(), anyString());
    }
    @Test
    void enviaMensajeIncorrecto() {
        HardwareConfig hc = new HardwareConfig("VPN", "RFC_5424", "CEF",
                "555.0.0.1", 8080, 10, 1);
        vpnService.processVPN(hc);

        verify(messageService, times(0)).sendMessage(anyString(), anyInt(), anyString());
    }
}