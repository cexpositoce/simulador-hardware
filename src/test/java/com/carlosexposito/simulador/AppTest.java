package com.carlosexposito.simulador;

import com.carlosexposito.App;
import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.firewall.service.FirewallService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;


@SpringBootTest
class AppTest {

    @Mock
    private FirewallService firewallService;

    @InjectMocks
    private App app;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testMain() {
        App.main(new String[]{});
        verify(firewallService, times(0)).processFirewall(any(HardwareConfig.class));
    }
    @Test
    void errorTipoHardware() {
        String[] args = {"origen", "formatoCabecera", "formatoCuerpo", "asdfasdf", "8080", "10", "1"};
        app.run(args);

        verify(firewallService, times(0)).processFirewall(any(HardwareConfig.class));
    }
    @Test
    void testOk() {
        String[] args = {"firewall", "formatoCabecera", "formatoCuerpo", "0.0.0.0", "8080", "0", "0"};
        app.run(args);

        verify(firewallService, times(0)).processFirewall(any(HardwareConfig.class));
    }

    @Test
    void errorFaltanArgumentos() {
        String[] args = {"not", "enough", "arguments"};
        app.run(args);

        verify(firewallService, never()).processFirewall(any(HardwareConfig.class));
    }

    @Test
    void testIsReachableUnknownHostException(){
        boolean result1 = App.isReachable("unknownhost");
        boolean result2 = App.isReachable("555.168.1.1");
        boolean result3 = App.isReachable(null);
        assertFalse(result1);
        assertFalse(result2);
        assertFalse(result3);
    }

    @Test
    void testIsReachableWithException() throws Exception {
        InetAddress inetAddressMock = mock(InetAddress.class);
        doThrow(new UnknownHostException("Test Exception")).when(inetAddressMock).isReachable(anyInt());

        // Spy en el método estático para devolver nuestro mock
        try (var mockedStatic = mockStatic(App.class, CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> App.getInetAddress(anyString())).thenReturn(inetAddressMock);

            boolean reachable = App.isReachable("127.0.0.1");
            assertFalse(reachable);
            // Asumimos que el logger funciona correctamente en el entorno, por lo que no necesitamos verificar la salida del logger en una prueba unitaria
        }
    }
}
