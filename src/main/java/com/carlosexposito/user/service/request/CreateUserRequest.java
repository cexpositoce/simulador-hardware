package com.carlosexposito.user.service.request;

public record CreateUserRequest (
    String name,
    String username,
    String password,
    String role
    )
{
}
