package com.carlosexposito.user.service.request;

public record AuthRequest (
    String username,
    String password
)
{

}