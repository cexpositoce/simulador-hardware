package com.carlosexposito.user.service.response;

public record UserResponse (String id, String username, String role) {
}
