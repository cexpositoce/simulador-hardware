package com.carlosexposito.user.controller;

import com.carlosexposito.security.JwtUtil;
import com.carlosexposito.user.controller.exceptions.UserNotFoundException;
import com.carlosexposito.user.model.User;
import com.carlosexposito.user.model.UserRepository;
import com.carlosexposito.user.service.UserDetailsServiceImpl;
import com.carlosexposito.user.service.request.AuthRequest;
import com.carlosexposito.user.service.request.CreateUserRequest;
import com.carlosexposito.user.service.response.UserResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.carlosexposito.config.Constants.*;

@RestController
@RequestMapping(API_AUTH)
public class AuthController {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;


    private final PasswordEncoder passwordEncoder;

    public AuthController(UserRepository userRepository, AuthenticationManager authenticationManager, JwtUtil jwtUtil, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(API_LOGIN)
    public ResponseEntity<String> login(@RequestBody AuthRequest authRequest) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.username(), authRequest.password()));
            final User user = userRepository.findByUsername(authRequest.username()).orElseThrow(UserNotFoundException::new);
            final String jwt = jwtUtil.generateToken(user.getUsername(), user.getRole());
            return ResponseEntity.ok(jwt);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(401).body(CREDENCIALES_INVALIDAS);
        }
    }

    @PreAuthorize("hasRole('Admin')")
    @PostMapping(API_REGISTER)
    public ResponseEntity<String> register(@RequestBody CreateUserRequest createUserRequest) {
        if (userRepository.findByUsername(createUserRequest.username()).isPresent()) {
            return ResponseEntity.status(400).body(ERROR_USUARIO_EXISTENTE);
        }

        User newUser = new User();
        newUser.setName(createUserRequest.name());
        newUser.setUsername(createUserRequest.username());
        newUser.setPassword(passwordEncoder.encode(createUserRequest.password()));
        newUser.setRole(createUserRequest.role());

        userRepository.save(newUser);
        return ResponseEntity.ok(USUARIO_REGISTRADO);
    }

    @PreAuthorize("hasRole('Admin')")
    @DeleteMapping(API_ID)
    public ResponseEntity<String> deleteUser(@PathVariable Long id) {
        if (!userRepository.existsById(id)) {
            return ResponseEntity.status(404).body(ERROR_USUARIO_NO_ENCONTRADO);
        }

        userRepository.deleteById(id);
        return ResponseEntity.ok(USUARIO_ELIMINADO);
    }

    @PreAuthorize("hasRole('Admin')")
    @GetMapping(API_ID)
    public UserResponse findUser(@PathVariable Long id) {
        User user = userRepository.findById(id).orElse(null);
        if(user == null) {
            throw new UserNotFoundException();
        }else {
            return new UserResponse(user.getId().toString(), user.getUsername(), user.getRole());
        }
    }

    @PreAuthorize("hasRole('Admin')")
    @GetMapping(API_ALL)
    public List<UserResponse> getAll() {
        List<User> user = userRepository.findAll();
        return user.stream().map(u -> new UserResponse(u.getId().toString(), u.getUsername(), u.getRole())).toList();
    }
}
