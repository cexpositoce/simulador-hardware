package com.carlosexposito;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.firewall.service.FirewallService;
import com.carlosexposito.hardware.vpn.service.VPNService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication(scanBasePackages = "com.carlosexposito")
@EnableJpaRepositories(basePackages = "com.carlosexposito")
@EntityScan("com.carlosexposito")
public class App implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final int TIMEOUT_MS = 5000;
    private static final int EXPECTED_ARGUMENTS = 7;


    private final FirewallService firewallService;
    private final VPNService vpnService;

    @Autowired
    public App(FirewallService firewallService, VPNService vpnService) {
        this.firewallService = firewallService;
        this.vpnService = vpnService;
    }


    private boolean validateArguments(String[] args) {
        if (args.length != EXPECTED_ARGUMENTS) {
            logger.error("Usage: java App <origen> <formatoCabecera> <formatoCuerpo> <direcciónIPDestino> <puertoDestino> <EPS> <modoEnvío>");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Override
    public void run(String... args) {
        if (!validateArguments(args)) return;

        HardwareConfig hc = new HardwareConfig(args[0], args[1], args[2], args[3], Integer.parseInt(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6]));

        boolean reachable = isReachable(hc.getDireccionIPDestino());
        logger.info("El servidor " + (reachable ? "es" : "no es") + " alcanzable en la dirección " + hc.getDireccionIPDestino() + ".");

        if ("firewall".equals(hc.getTipoHardware().toLowerCase())) firewallService.processFirewall(hc);
        if ("vpn".equals(hc.getTipoHardware().toLowerCase())) vpnService.processVPN(hc);

    }

    public static boolean isReachable(String ipAddress) {
        try {
            InetAddress inetAddress = getInetAddress(ipAddress);
            if(inetAddress == null) throw new UnknownHostException();
            return inetAddress.isReachable(TIMEOUT_MS);
        } catch (UnknownHostException e) {
            logger.error("Host desconocido: {}", ipAddress);
        } catch (Exception e) {
            logger.error("Error al realizar el ping a: {} - {}", ipAddress, e.getMessage());
        }
        return false;
    }

    public static InetAddress getInetAddress(String ipAddress) {
        if(ipAddress == null) return null;
        try {
            return InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            logger.error("Host desconocido: {}", ipAddress);
        }
        return null;
    }
}