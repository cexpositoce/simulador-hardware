package com.carlosexposito.hardware.firewall.model;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.Hardware;
import com.carlosexposito.hardware.message.MessageService;


public class Firewall extends Hardware {
    public Firewall(MessageService messageService, HardwareConfig hc) {
        super(hc.getFormatoCabecera(), hc.getFormatoCuerpo(), hc.getDireccionIPDestino(), hc.getPuertoDestino(), hc.getEPS(), hc.getModoEnvio(), messageService);
    }
}