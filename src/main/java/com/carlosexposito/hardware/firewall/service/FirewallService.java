package com.carlosexposito.hardware.firewall.service;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.firewall.model.Firewall;
import com.carlosexposito.hardware.message.LogGenerator;
import com.carlosexposito.hardware.message.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.carlosexposito.App;
import com.carlosexposito.config.HeaderFormat;
import com.carlosexposito.config.BodyType;

import static com.carlosexposito.config.Constants.*;

@Service
public class FirewallService {

    private static final Logger logger = LoggerFactory.getLogger(FirewallService.class);

    private final MessageService messageService;

    public FirewallService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void processFirewall(HardwareConfig hc) {
        Firewall firewall = new Firewall(messageService, hc);
        logger.info(FIREWALL_PROCESANDO+"{}", hc);

        if (!App.isReachable(firewall.getDireccionIPDestino())) {
            logger.warn(ERROR_IP_NO_ALCANZABLE+"{}", firewall.getDireccionIPDestino());
            return;
        }

        mostrarEstado(firewall);

        for (int i = 0; i != firewall.getModoEnvio(); i++) {
            String logMessage = LogGenerator.generateLog("Firewall",HeaderFormat.valueOf(firewall.getFormatoCabecera()), BodyType.valueOf(firewall.getFormatoCuerpo()), i);
            firewall.sendMessage(":"+logMessage);
            try {
                Thread.sleep(1000 / firewall.getEPS());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logger.error(ERROR_INTERRUPCION+"{}", e.getMessage());
                return;
            }
        }
    }

    public void mostrarEstado(Firewall firewall) {
        logger.info("Estado actual del firewall:");
        logger.info("Formato de cabecera: " + firewall.getFormatoCabecera());
        logger.info("Formato de cuerpo: " + firewall.getFormatoCuerpo());
        logger.info("Dirección IP de destino: " + firewall.getDireccionIPDestino());
        logger.info("Puerto de destino: " + firewall.getPuertoDestino());
        logger.info("Tasa de eventos por segundo (EPS): " + firewall.getEPS());
        logger.info("Modo de envío: " + firewall.getModoEnvio());
    }
}