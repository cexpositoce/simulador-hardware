package com.carlosexposito.hardware.firewall.controller;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.firewall.service.FirewallService;
import com.carlosexposito.registry.model.Registry;
import com.carlosexposito.registry.model.RegistryRepository;
import com.carlosexposito.user.model.User;
import com.carlosexposito.user.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.carlosexposito.config.Constants.*;

@RestController
public class FirewallController {


    private final FirewallService firewallService;

    private final RegistryRepository registryRepository;

    private final UserRepository userRepository;

    @Autowired
    public FirewallController(FirewallService firewallService, RegistryRepository registryRepository, UserRepository userRepository) {
        this.firewallService = firewallService;
        this.registryRepository = registryRepository;
        this.userRepository = userRepository;
    }

    @PreAuthorize("hasRole('Admin,Mantainer,User')")
    @GetMapping(API_FIREWALL)
    public String generateFirewallLog(
            @RequestParam String formatoCabecera,
            @RequestParam String formatoCuerpo,
            @RequestParam String direccionIPDestino,
            @RequestParam int puertoDestino,
            @RequestParam int EPS,
            @RequestParam int modoEnvio) {


        HardwareConfig hc = new HardwareConfig(HARDWARE_FIREWALL, formatoCabecera, formatoCuerpo,
                direccionIPDestino, puertoDestino, EPS, modoEnvio);


        firewallService.processFirewall(hc);
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findByUsername(principal.getUsername()).orElse(null);
        Registry registry = new Registry();
        registry.setIdUser(user.getId());
        registry.setHardware(HARDWARE_FIREWALL);
        registry.setHeaderType(formatoCabecera);
        registry.setBody(formatoCuerpo);
        registry.setIp(direccionIPDestino);
        registry.setPort(String.valueOf(puertoDestino));
        registry.setEps(String.valueOf(EPS));
        registry.setSending(String.valueOf(modoEnvio));
        registryRepository.save(registry);
        return LOG_FIREWALL_OK;
    }
}