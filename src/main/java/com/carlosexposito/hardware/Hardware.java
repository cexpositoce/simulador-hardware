package com.carlosexposito.hardware;

import com.carlosexposito.hardware.message.MessageService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

@Getter
@Setter
public abstract class Hardware {
    private String formatoCabecera;
    private String formatoCuerpo;
    private String direccionIPDestino;
    private int puertoDestino;
    private int EPS;
    private int modoEnvio;
    private MessageService messageService;

    @Autowired
    protected Hardware(String formatoCabecera, String formatoCuerpo, String direccionIPDestino, int puertoDestino, int EPS, int modoEnvio, MessageService messageService) {
        this.formatoCabecera = formatoCabecera;
        this.formatoCuerpo = formatoCuerpo;
        this.direccionIPDestino = direccionIPDestino;
        this.puertoDestino = puertoDestino;
        this.EPS = EPS;
        this.modoEnvio = modoEnvio;
        this.messageService = messageService;
    }

    @Autowired
    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }


    public void sendMessage(String message) {
        if (messageService != null) {
            messageService.sendMessage(direccionIPDestino, puertoDestino, message);
        }
    }
}