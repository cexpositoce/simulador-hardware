package com.carlosexposito.hardware.message;

public interface MessageService {
    void sendMessage(String direccionIPDestino, int puertoDestino, String message);
}
