package com.carlosexposito.hardware.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static com.carlosexposito.config.Constants.*;

@Service
public class MessageSenderService implements MessageService{

    private static final Logger logger = LoggerFactory.getLogger(MessageSenderService.class);
    @Override
    public void sendMessage(String direccionIPDestino, int puertoDestino, String message) {
        try (DatagramSocket socket = createDatagramSocket()) {
            InetAddress serverAddress = InetAddress.getByName(direccionIPDestino);
            byte[] buffer = message.getBytes();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, serverAddress, puertoDestino);
            socket.send(packet);
            logger.info(MENSAJE_ENVIADO_OK + "{}:{}", direccionIPDestino, puertoDestino);
        } catch (Exception e) {
            logger.error(MENSAJE_ENVIADO_ERROR + "{}:{} - {}", direccionIPDestino, puertoDestino, e.getMessage());
        }
    }

    @SuppressWarnings("java:S112")
    protected DatagramSocket createDatagramSocket() throws Exception{
        return new DatagramSocket();
    }
}
