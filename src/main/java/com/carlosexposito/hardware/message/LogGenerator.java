package com.carlosexposito.hardware.message;

import com.carlosexposito.config.BodyType;
import com.carlosexposito.config.HeaderFormat;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogGenerator {
    private static final SecureRandom RANDOM = new SecureRandom();

    private LogGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static String generateLog(String type,HeaderFormat header, BodyType body, int index) {
        StringBuilder logMessage = new StringBuilder();

        if (header == HeaderFormat.RFC_3164) {
            logMessage.append(generateRFC3164Header());
        } else if (header == HeaderFormat.RFC_5424) {
            logMessage.append(generateRFC5424Header(type));
        }

        if(body == BodyType.CEF){
            logMessage.append(generateCEFBody(type, index));
        }else if(body == BodyType.LEF){
            logMessage.append(generateLEFBody(index));
        }
        else if(body == BodyType.KEY_VALUE){
            logMessage.append(generateKeyValueBody(index));
        }
        else if(body == BodyType.APACHE_LOG){
            logMessage.append(generateApacheLogBody(index));
        }

        return logMessage.toString();
    }

    private static String generateRFC3164Header() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd HH:mm:ss");
        String timestamp = dateFormat.format(new Date());
        String hostname = generateRandomIP();
        return String.format(" 34 %s %s ", timestamp, hostname);
    }

    public static String generateRFC5424Header(String type) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String timestamp = dateFormat.format(new Date());
        String hostname = generateRandomIP();
        String appName = "SimuladorHW"+type;
        String procId = String.valueOf(RANDOM.nextInt(1000));
        String msgId = "ID" + RANDOM.nextInt(100);
        return String.format("<34>1 %s %s %s %s %s - ", timestamp, hostname, appName, procId, msgId);
    }

    public static String generateCEFBody(String type,int index) {
        String deviceVendor = "DeviceVendor";
        String deviceProduct = "DeviceProduct";
        String deviceVersion = "1.0";
        String signatureId = "1000";
        String name = type+" Log";
        String severity = "Low";
        return escapeSpecialCharacters(String.format("CEF 0 %s %s %s %s %s %s msg=%s",
                deviceVendor, deviceProduct, deviceVersion, signatureId, name, severity, "Log message number " + index));
    }

    private static String generateLEFBody(int index) {
        return String.format("LEF: Log message number %d", index);
    }

    private static String generateKeyValueBody(int index) {
        return String.format("message=Log message number %d, sourceIP=%s, destinationIP=%s",
                index, generateRandomIP(), generateRandomIP());
    }

    private static String generateApacheLogBody(int index) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss Z");
        String timestamp = dateFormat.format(new Date());
        return String.format("%s - - [%s] \"GET / HTTP/1.1\" 200 %d",
                generateRandomIP(), timestamp, index * 100);
    }

    private static String escapeSpecialCharacters(String message) {
        // Escapar caracteres especiales comunes
        return message.replace("\\", "\\\\")
                .replace("\"", "\\\"")
                .replace("/", "\\/")
                .replace("\n", "\\n")
                .replace("\r", "\\r");
    }

    public static String generateRandomIP() {
        return RANDOM.nextInt(256) + "." +
                RANDOM.nextInt(256) + "." +
                RANDOM.nextInt(256) + "." +
                RANDOM.nextInt(256);
    }

}
