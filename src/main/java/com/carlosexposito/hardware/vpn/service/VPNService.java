package com.carlosexposito.hardware.vpn.service;

import com.carlosexposito.App;
import com.carlosexposito.config.BodyType;
import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.config.HeaderFormat;
import com.carlosexposito.hardware.message.LogGenerator;
import com.carlosexposito.hardware.message.MessageService;
import com.carlosexposito.hardware.vpn.model.VPN;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.carlosexposito.config.Constants.*;

@Service
public class VPNService {

    private static final Logger logger = LoggerFactory.getLogger(VPNService.class);

    private final MessageService messageService;

    public VPNService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void processVPN(HardwareConfig hc) {
        VPN vpn = new VPN(messageService, hc);
        logger.info(VPN_PROCESANDO+"{}", hc);

        if (!App.isReachable(vpn.getDireccionIPDestino())) {
            logger.warn(ERROR_IP_NO_ALCANZABLE+"{}", vpn.getDireccionIPDestino());
            return;
        }

        mostrarEstado(vpn);

        for (int i = 0; i != vpn.getModoEnvio(); i++) {
            String logMessage = LogGenerator.generateLog("VPN",HeaderFormat.valueOf(vpn.getFormatoCabecera()), BodyType.valueOf(vpn.getFormatoCuerpo()), i);
            vpn.sendMessage(":"+logMessage);
            try {
                Thread.sleep(1000 / vpn.getEPS());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logger.error(ERROR_INTERRUPCION+"{}", e.getMessage());
                return;
            }
        }
    }

    public void mostrarEstado(VPN vpn) {
        logger.info("Estado actual del VPN:");
        logger.info("Formato de cabecera: " + vpn.getFormatoCabecera());
        logger.info("Formato de cuerpo: " + vpn.getFormatoCuerpo());
        logger.info("Dirección IP de destino: " + vpn.getDireccionIPDestino());
        logger.info("Puerto de destino: " + vpn.getPuertoDestino());
        logger.info("Tasa de eventos por segundo (EPS): " + vpn.getEPS());
        logger.info("Modo de envío: " + vpn.getModoEnvio());
    }
}