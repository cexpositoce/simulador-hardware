package com.carlosexposito.hardware.vpn.model;

import com.carlosexposito.config.HardwareConfig;
import com.carlosexposito.hardware.Hardware;
import com.carlosexposito.hardware.message.MessageService;


public class VPN extends Hardware {
    public VPN(MessageService messageService, HardwareConfig hc) {
        super(hc.getFormatoCabecera(), hc.getFormatoCuerpo(), hc.getDireccionIPDestino(), hc.getPuertoDestino(), hc.getEPS(), hc.getModoEnvio(), messageService);
    }
}