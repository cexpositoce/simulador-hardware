package com.carlosexposito.config;

public enum HeaderFormat {
    RFC_3164,
    RFC_5424
}
