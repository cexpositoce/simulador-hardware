package com.carlosexposito.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import static com.carlosexposito.config.Constants.*;

@Getter
@Setter
@AllArgsConstructor
public class HardwareConfig {

    @NotBlank(message = ERROR_HARDWARE_TIPO_VACIO)
    private String tipoHardware;

    private String formatoCabecera;

    private String formatoCuerpo;

    @NotBlank(message = ERROR_HARDWARE_IP_VACIA)
    private String direccionIPDestino;

    @Min(value= 1, message = ERROR_HARDWARE_PUERTO)
    private int puertoDestino;

    @Min(value= 1, message = ERROR_HARDWARE_EPS)
    private int EPS;

    @Min(value= -1, message = ERROR_HARDWARE_MODO_ENVIO)
    private int modoEnvio;
}
