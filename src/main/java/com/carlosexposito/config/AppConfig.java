package com.carlosexposito.config;

import com.carlosexposito.hardware.firewall.model.Firewall;
import com.carlosexposito.hardware.message.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.carlosexposito.config.Constants.*;

@Configuration
public class AppConfig {

    private final MessageService messageService;

    @Autowired
    public AppConfig(MessageService messageService) {
        this.messageService = messageService;
    }

    @Bean
    public HardwareConfig hardwareConfig() {
        return new HardwareConfig(HARDWARE_TIPO, HARDWARE_FORMATO_CABECERA, HARDWARE_FORMATO_CUERPO,
                HARDWARE_DIRECCION_IP, 8080, 100, 1);
    }

    @Bean
    public Firewall firewall(HardwareConfig hardwareConfig) {
        return new Firewall(messageService, hardwareConfig);
    }

}
