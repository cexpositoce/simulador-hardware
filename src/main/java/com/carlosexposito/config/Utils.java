package com.carlosexposito.config;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Utils {

    @GetMapping("/ping")
    public String ping() {
        return "pong";
    }
}
