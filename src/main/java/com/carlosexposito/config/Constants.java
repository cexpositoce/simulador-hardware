package com.carlosexposito.config;

public final class Constants {

    //Constructor
    private Constants() {
    }

    //Tables
    public static final String TABLE_USER = "users";
    public static final String TABLE_REGISTRIES = "registries";
    //Variables
    public static final String HARDWARE_FIREWALL = "Firewall";
    public static final String HARDWARE_VPN = "VPN";
    public static final String HARDWARE_TIPO = "tipoHardware";
    public static final String HARDWARE_FORMATO_CABECERA = "formatoCabecera";
    public static final String HARDWARE_FORMATO_CUERPO = "formatoCuerpo";
    public static final String HARDWARE_DIRECCION_IP = "direccionIPDestino";

    //Llamadas API
    public static final String API_FIREWALL = "/firewall";
    public static final String API_VPN = "/VPN";
    public static final String API_AUTH = "/auth";
    public static final String API_LOGIN = "/login";
    public static final String API_REGISTER = "/register";
    public static final String API_ID = "/{id}";
    public static final String API_ALL = "/all";

    //Respuestas
    public static final String LOG_FIREWALL_OK = "Log de Firewall generado correctamente.";
    public static final String LOG_VPN_OK = "Log de VPN generado correctamente.";
    public static final String ERROR_IP_NO_ALCANZABLE = "Dirección IP no es alcanzable: ";
    public static final String FIREWALL_PROCESANDO = "Procesando firewall con configuración: ";
    public static final String VPN_PROCESANDO = "Procesando VPN con configuración: ";
    public static final String ERROR_INTERRUPCION = "Interrupción en generateLog: ";
    public static final String ERROR_HARDWARE_TIPO_VACIO = "El tipo de hardware no puede estar vacío";
    public static final String ERROR_HARDWARE_IP_VACIA = "La dirección IP de destino no puede estar vacía";
    public static final String ERROR_HARDWARE_PUERTO = "El puerto de destino debe ser mayor que 0";
    public static final String ERROR_HARDWARE_EPS = "La tasa de eventos por segundo (EPS) debe ser mayor que 0";
    public static final String ERROR_HARDWARE_MODO_ENVIO = "El modo de envío debe ser mayor que -1";
    public static final String MENSAJE_ENVIADO_OK = "Mensaje enviado con éxito a ";
    public static final String MENSAJE_ENVIADO_ERROR = "Error al enviar el mensaje a ";
    public static final String CREDENCIALES_INVALIDAS = "Credenciales inválidas";
    public static final String ERROR_USUARIO_EXISTENTE = "El usuario ya existe";
    public static final String USUARIO_REGISTRADO = "Usuario registrado correctamente";
    public static final String ERROR_USUARIO_NO_ENCONTRADO = "Usuario no encontrado";
    public static final String USUARIO_ELIMINADO = "Usuario eliminado exitosamente";
}
