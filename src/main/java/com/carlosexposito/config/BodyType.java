package com.carlosexposito.config;

public enum BodyType {
    CEF,
    LEF,
    KEY_VALUE,
    APACHE_LOG
}