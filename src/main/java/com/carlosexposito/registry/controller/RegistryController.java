package com.carlosexposito.registry.controller;

import com.carlosexposito.registry.model.Registry;
import com.carlosexposito.registry.model.RegistryRepository;
import com.carlosexposito.registry.service.response.RegistryResponse;
import com.carlosexposito.user.model.User;
import com.carlosexposito.user.model.UserRepository;
import com.carlosexposito.user.service.response.UserResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RegistryController {

    private final RegistryRepository registryRepository;

    private final UserRepository userRepository;

    public RegistryController(RegistryRepository registryRepository, UserRepository userRepository) {
        this.registryRepository = registryRepository;
        this.userRepository = userRepository;
    }

    @PreAuthorize("hasRole('Mantainer')")
    @GetMapping("/registries")
    public List<RegistryResponse> getAll() {
        List<Registry> registry = registryRepository.findAll();
        List<RegistryResponse> registryResponse = new ArrayList<>(List.of());
        for (Registry r : registry) {
            User user;
            if (userRepository.existsById(r.getIdUser())) {
                user = userRepository.findById(r.getIdUser()).orElse(null);
            } else {
                user = new User();
                user.setId(0L);
                user.setUsername("Deleted");
                user.setRole("Deleted");
            }

            UserResponse userResponse = new UserResponse(user.getId().toString(), user.getUsername(), user.getRole());
            registryResponse.add(new RegistryResponse(r.getId(), userResponse, r.getHardware(), r.getHeaderType(), r.getBody(), r.getIp(), r.getPort(), r.getEps(), r.getSending()));
        }
        return registryResponse;
    }
}
