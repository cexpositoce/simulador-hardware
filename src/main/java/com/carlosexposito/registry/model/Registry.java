package com.carlosexposito.registry.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import static com.carlosexposito.config.Constants.*;

@Entity
@Table(name = TABLE_REGISTRIES)
@Getter
@Setter
public class Registry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "id_user")
    private Long idUser;
    private String hardware;
    @Column(name = "header_type")
    private String headerType;
    private String body;
    private String ip;
    private String port;
    private String eps;
    private String sending;
}