package com.carlosexposito.registry.service.response;

import com.carlosexposito.user.service.response.UserResponse;

public record RegistryResponse(Long id, UserResponse user, String hardware, String headerType, String body, String ip, String port, String eps, String sending) {
}
