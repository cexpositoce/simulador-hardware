CREATE SCHEMA IF NOT EXISTS simulador;

SET search_path TO simulador;

CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    username VARCHAR(255),
    password VARCHAR(255),
    role VARCHAR(255)
    );

INSERT INTO users (name, username, password, role)
VALUES ('Administrador', 'admin', 'MyP@$$', 'Admin');

CREATE TABLE simulador.registries (
      id bigserial NOT NULL,
      id_user int8 NOT NULL,
      hardware varchar NULL,
      header_type varchar NULL,
      body varchar NULL,
      ip varchar NULL,
      port varchar NULL,
      eps varchar NULL,
      sending varchar NULL,
      CONSTRAINT registries_pk PRIMARY KEY (id)
);

ALTER TABLE simulador.registries
    ADD CONSTRAINT registries_fk_user
        FOREIGN KEY (id_user) REFERENCES users(id);