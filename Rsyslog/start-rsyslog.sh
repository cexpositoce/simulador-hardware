#!/bin/bash

echo "La dirección IP de este contenedor es: $(hostname -I | cut -d' ' -f1)"

rsyslogd -n &

sleep 5

if [ ! -f /var/log/syslog ]; then
    echo "Esperando a que Rsyslog genere /var/log/syslog..."
    sleep 5
fi

if [ -f /var/log/syslog ]; then
    tail -f /var/log/syslog
else
    echo "Archivo /var/log/syslog no encontrado, continuando sin seguir logs."
    while true; do sleep 1; done
fi