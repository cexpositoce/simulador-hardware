import { Routes, provideRouter } from '@angular/router';
import { MainComponent } from './pages/main/main/main.component';
import { UserpanelComponent } from './pages/userpanel/userpanel.component';
import { AdminpanelComponent } from './pages/adminpanel/adminpanel.component';
import { MantainerPanelComponent } from './pages/mantainer-panel/mantainer-panel.component';
import { authGuard } from './guards/auth.guard';



export const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path : 'login', component: MainComponent },
    { path : 'admin-panel', component: AdminpanelComponent,  canActivate: [authGuard]},
    { path : 'user-panel', component: UserpanelComponent,  canActivate: [authGuard]},
    { path : 'mantainer-panel', component: MantainerPanelComponent, canActivate: [authGuard]},
    { path: '**', redirectTo: 'login' }
];

