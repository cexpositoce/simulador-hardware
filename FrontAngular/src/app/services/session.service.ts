import { Injectable } from '@angular/core';
import { JwtDecoderService } from './jwt-decoder.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private jwtService : JwtDecoderService) { }

  setSession( key: string, value: string ){
    sessionStorage.setItem( key , value );
  }

  getSession(){
    return sessionStorage;
  }

  clearSession(){
    sessionStorage.clear();
  }

  removeSession(key: string){
    sessionStorage.removeItem( key );
  }
  
  
}

