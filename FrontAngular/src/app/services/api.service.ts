import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { INewUser, IResponseRegistries, IUser } from '../interfaces/api.interface';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  private baseUrl: string = 'http://13.50.172.168:8080/';

  public ipSelected: string = '';

  setIpSelected(ip: string){
    this.ipSelected = ip;
    console.log('Cambiado ip')
  }

  constructor(private http: HttpClient) { }



  login(username: string, password: string): Observable<string> {
    const url = `${this.baseUrl}${'auth/login'}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(url, { username, password }, { headers, responseType: 'text' }).pipe(
      tap(token => localStorage.setItem('token', token))
    );
  }

  logout() {
    localStorage.removeItem('token');
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }


  getAllUsers(): Observable<IUser[]> {
    const url = `${this.baseUrl}${'auth/all'}`;
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.getToken()}` 
    });

    return this.http.get<IUser[]>(url, { headers });
  }

  registerUser(user: INewUser): Observable<string> {
    const url = `${this.baseUrl}${'auth/register'}`;
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.getToken()}` 
    });

    return this.http.post<string>(url, user, { headers, responseType: 'text' as 'json' });
  }

  deleteUser(id: number): Observable<IUser> {
    const url = `${this.baseUrl}${'auth/'}${id}`;
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.getToken()}` 
    });
    return this.http.delete<IUser>(url, { headers , responseType: 'text' as 'json'});
  }

  ping( ip : string ): Observable<string> {
    const url = `http://${ip}:8080/${'ping'}`;
    console.log(ip, url)
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.getToken()}` 
    });
    return this.http.get<string>(url, { headers , responseType: 'text' as 'json'});
  }


  createPetition( hardware: string, header: string, body: string, ip: string, port: number, eps: number, sendMode: number){
    const url = ` http://${this.ipSelected}:8080/${hardware}?tipoHardware=${hardware}&formatoCabecera=${header}&formatoCuerpo=${body}&direccionIPDestino=${ip}&puertoDestino=${port}&EPS=${eps}&modoEnvio=${sendMode}`;
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.getToken()}` 
    });
    console.log(url)
    return this.http.get<string>(url, { headers , responseType: 'text' as 'json'});
  }

  getAllRegistries(): Observable<IResponseRegistries[]> {
    const url = `${this.baseUrl}${'registries'}`;
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.getToken()}` 
    });

    return this.http.get<IResponseRegistries[]>(url, { headers });
  }

}
