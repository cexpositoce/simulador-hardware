import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { JwtDecoderService } from '../../../services/jwt-decoder.service';
import { Router, RouterModule } from '@angular/router';
import { SessionService } from '../../../services/session.service';
import { Role } from '../../../interfaces/api.interface';


@Component({
  selector: 'app-main',
  standalone: true,
  imports: [FormsModule, RouterModule],
  templateUrl: './main.component.html',
  styleUrl: './main.component.css'
})
export class MainComponent {

  username : string = '';
  password : string = '';
  error : string = '';

  constructor(private apiService: ApiService, private jwtDecoder : JwtDecoderService, private router: Router, private sessionService : SessionService) { }


  login() {
    this.apiService.login(this.username, this.password).subscribe({
      next: (data) => {
        console.log(data);
        const token = data; 
        const decodedToken = this.jwtDecoder.decodeToken(data);
        this.sessionService.setSession('role', decodedToken.role);
        console.log(decodedToken);
        switch (decodedToken.role) {
          case Role.Admin:
            this.router.navigate(['/admin-panel']);
            break;
          case Role.User:
            this.router.navigate(['/user-panel']);
            break;
          case Role.Mantainer:
            this.router.navigate(['/mantainer-panel']);
            break;
          default:
            setTimeout(() => {
              this.error = '';
            }, 3000);
            break;
        }
      },
      error: (error) => {
        console.log(error);
        this.error = error.error;
        setTimeout(() => {
          this.error = '';
        }, 3000);
      },
      complete: () => {
        console.log('Login complete');
      }
    });
  }
  

}
