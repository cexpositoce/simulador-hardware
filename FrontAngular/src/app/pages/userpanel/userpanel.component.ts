import { Component } from '@angular/core';
import { BodyType, Hardware, HeaderFormart, ISendConfig, Role } from '../../interfaces/api.interface';
import { FormsModule } from '@angular/forms';
import { AnimationOptions, LottieComponent } from 'ngx-lottie';
import { AnimationItem } from 'ngx-lottie/lib/symbols';
import { ApiService } from '../../services/api.service';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { SessionService } from '../../services/session.service';
import { JwtDecoderService } from '../../services/jwt-decoder.service';
import { PanelMenuComponent } from "../../shared/panel-menu/panel-menu.component";


@Component({
    selector: 'app-userpanel',
    standalone: true,
    templateUrl: './userpanel.component.html',
    styleUrl: './userpanel.component.css',
    imports: [CommonModule, FormsModule, LottieComponent, PanelMenuComponent]
})
export class UserpanelComponent {


   statusLight : boolean = false;
   isLoading : boolean = false;
   completeProcess : boolean = false;
   avaibleButton: boolean = false;
   error: string = '';
   ipSelected : string = '';
   actualPanel : string = 'user-panel';
   actualRole : Role = Role.User;
   isInfoModalOpen : boolean = false;
   
   infoHoverVisible: boolean = false;
   infoHoverPosition: number = 0;
   infoHoverText: string = '';

   showInfoModal = false;
   
  toggleInfoHover(title: string) {
    if(this.infoHoverVisible){
      this.infoHoverVisible = false;
    }else{
      switch(title){
        case 'hardware': 
          this.infoHoverText = 'Hardware es el tipo de dispositivo que se va a utilizar para enviar la petición.';
          this.infoHoverPosition = 21;
          break;
        case 'header':
          this.infoHoverText = 'Header es el formato que se va a utilizar para enviar la petición. Puede ser un formato RFC_3164 o un formato RFC_5424.';
          this.infoHoverPosition = 31;
          break;
        case 'body':
          this.infoHoverText = 'Body es el tipo de cuerpo que se va a utilizar para enviar la petición. Puede ser un cuerpo CEF, un cuerpo LEF, un cuerpo KEY_VALUE o un cuerpo APACHE_LOG.';
          this.infoHoverPosition = 41;
          break;
        case 'ip':
          this.infoHoverText = 'IP es la dirección IP del dispositivo al que se le va a enviar la petición.';
          this.infoHoverPosition = 51;
          break;
        case 'port':
          this.infoHoverText = 'Port es el puerto al que se le va a enviar la petición.';
          this.infoHoverPosition = 61;
          break;
        case 'eps':
          this.infoHoverText = 'EPS es el número de eventos por segundo que se van a enviar.';
          this.infoHoverPosition = 71;
          break;
        case 'sendMode':
          this.infoHoverText = 'SendMode es el modo de envío que se va a utilizar para enviar la petición. Puede ser un modo de envío sincrónico o un modo de envío asíncrono.';
          this.infoHoverPosition = 81;
          break;
      }
      this.infoHoverVisible = true;
    }
  }


   openInfoModal(){
    this.isInfoModalOpen = true;
   }

   ngOnInit(): void {
    let session  = this.sessionService.getSession();
    this.actualRole = session['role'];
   }

  sendConnectionConfig : ISendConfig = {
    hardware: undefined,
    header: undefined,
    body: undefined,
    ip: '',
    port: undefined,
    eps: undefined,
    sendMode: undefined
  }

  hardwareEnum = Object.values(Hardware);
  headerFormartEnum = Object.values(HeaderFormart);
  bodyTypeEnum = Object.values(BodyType);

  
  options: AnimationOptions = {
    path: 'assets/hardwareAnim.json'
  };

  optionsSpinner: AnimationOptions = {
    path: 'assets/spinner.json'
  };

  optionsDone: AnimationOptions = {
    path: 'assets/done.json'

  };
  
  animationCreated(animationItem: AnimationItem): void {
    animationItem.addEventListener('complete', () => {
      console.log('Animation completed');
    });
  }

  constructor(private apiService : ApiService, private router: Router, private sessionService: SessionService, private jwtDecoderService: JwtDecoderService) {
}
  
ping(){
    this.apiService.setIpSelected(this.ipSelected);
    this.avaibleButton = true;
    this.apiService.ping(this.ipSelected).subscribe({
      next: (data) => {
        console.log(data);
        this.statusLight = true;
        setTimeout(() => {
           this.ping();
        }, 30000);
      },
      error: (error) => {
        console.log(error);
        this.statusLight = false;
        this.avaibleButton = false;
      },
      complete: () => {
      }
    });
  }

  createPetition(){
    if(this.sendConnectionConfig.hardware === undefined || this.sendConnectionConfig.header === undefined || this.sendConnectionConfig.body === undefined || this.sendConnectionConfig.ip === '' || this.sendConnectionConfig.port === undefined || this.sendConnectionConfig.eps === undefined || this.sendConnectionConfig.sendMode === undefined){
      this.error = 'Todo los campos son requeridos'
      setTimeout(() => {
        this.error = '';
      }, 3000);
      return;
    }
    this.isLoading = true;
    this.apiService.createPetition(this.sendConnectionConfig.hardware!, this.sendConnectionConfig.header!, this.sendConnectionConfig.body!, this.sendConnectionConfig.ip, this.sendConnectionConfig.port!, this.sendConnectionConfig.eps!, this.sendConnectionConfig.sendMode!).subscribe({
      next: (data) => {
        setTimeout(() => {
          this.isLoading = false;
          this.completeProcess = true;
          setTimeout(() => {
            this.completeProcess = false;
          }, 3000);
        }, 1000);
        
        
      },
      error: (error) => {
        if(error.status === 401 || error.status === 403 || error.status === 500){
          this.router.navigate(['/']);
        
        }
        console.log(error);
        this.isLoading = false;

      },
      complete: () => {
        console.log('Petition complete');
      }
    });
  }
  logout(){
    this.apiService.logout();
    this.router.navigate(['/']);
  }

}
