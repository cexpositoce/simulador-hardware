import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { IResponseRegistries, Role } from '../../interfaces/api.interface';
import { Router } from '@angular/router';
import { JwtDecoderService } from '../../services/jwt-decoder.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { PanelMenuComponent } from "../../shared/panel-menu/panel-menu.component";
import { SessionService } from '../../services/session.service';

@Component({
    selector: 'app-mantainer-panel',
    standalone: true,
    templateUrl: './mantainer-panel.component.html',
    styleUrl: './mantainer-panel.component.css',
    imports: [NgxPaginationModule, PanelMenuComponent]
})
export class MantainerPanelComponent {

  allRegistries : IResponseRegistries[] = [];
  //Paginacion
  p: number = 1;
  total: number = 0;

  actualPanel: string = 'mantainer-panel';
  actualRole : Role = Role.User;

  constructor(private apiService : ApiService, private router : Router, private jwt : JwtDecoderService, private sessionService : SessionService){}

  ngOnInit(): void {
    let session  = this.sessionService.getSession();
    this.actualRole = session['role'];
    console.log(this.actualRole);

    this.apiService.getAllRegistries().subscribe({
      next: (data : IResponseRegistries[]) => {
        this.allRegistries = data;
        this.total = data.length;
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        console.log('Get all registries complete');
      }
    });
  }


  logout(){
    this.apiService.logout();
    this.router.navigate(['/']);
  }

}
