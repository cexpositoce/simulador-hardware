import { Component, OnChanges, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { INewUser, IUser, Role } from '../../interfaces/api.interface';
import { FormsModule } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { JwtDecoderService } from '../../services/jwt-decoder.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { PanelMenuComponent } from "../../shared/panel-menu/panel-menu.component";

@Component({
    selector: 'app-adminpanel',
    standalone: true,
    templateUrl: './adminpanel.component.html',
    styleUrl: './adminpanel.component.css',
    imports: [FormsModule, NgxPaginationModule, RouterModule, PanelMenuComponent]
})
export class AdminpanelComponent implements OnInit {

  constructor(private apiService: ApiService, private router : Router, private jwt : JwtDecoderService ) { }

  
  users : IUser[] = [];

  //Paginacion
  p : number = 1;
  total: number = 0;

  actualPanel : string = 'admin-panel';

  newUser : INewUser = {
    name: '',
    username: '',
    password: '',
    role: undefined
  }

  error: string = '';

  ngOnInit(): void {
    this.getUsers();
  
  }

  getUsers(){
    this.apiService.getAllUsers().subscribe({
      next: (data) => {
        this.users = data;
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        console.log('Get users complete');
      }
    });
  }

  addUser(){
    this.apiService.registerUser(this.newUser).subscribe({
      next: (data) => {
        this.getUsers();
      },
      error: (error) => {
        console.log(error);
        this.error = error.error;
      },
      complete: () => {
        console.log('Add user complete');
        this.newUser = {
          name: '',
          username: '',
          password: '',
          role: undefined
        }
      }
    });
  }

  deleteUser(id: number) {
    this.apiService.deleteUser(id).subscribe({
      next: (data) => {
        this.getUsers();
      },
      error: (error) => {
        console.log('ERROR',error.error);
        
      },
      complete: () => {
        console.log('Delete user complete');
      }
    });
  }

  logout(){
    this.apiService.logout();
    this.router.navigate(['/']);
  }
}
