import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { JwtDecoderService } from '../services/jwt-decoder.service';
import { Role } from '../interfaces/api.interface';

export const authGuard: CanActivateFn = (route, state) => {
  const apiService = inject(ApiService);
  const jwtHelper = inject(JwtDecoderService);
  const router = inject(Router);

  const token = apiService.getToken();
  if(token){
    const decodedToken = jwtHelper.decodeToken(token);
    const role = decodedToken.role;
    switch(role){
      case Role.Admin:
        return true;
      case Role.User:
        if(route.url.toString() === 'user-panel'){
          return true;
        }
        router.navigate(['/']);
        return false;
      case Role.Mantainer:
        if(route.url.toString() === 'mantainer-panel' || route.url.toString() === 'user-panel'){
          return true;
        }
        router.navigate(['/']);
        return false;
      default:
        router.navigate(['/']);
        return false;
    }
  
  } else {
    router.navigate(['/']);
    return false;
  }
  
 
};
