export interface ILogin{
    username: string;
    password: string;
}

export interface IUser{
    id: number;
    username: string;
    role: Role | undefined;
}

export interface INewUser{
    name : string;
    username: string;
    password: string;
    role: Role | undefined;
}



export enum Role {
    Admin = 'Admin',
    User = 'User',
    Mantainer = 'Mantainer'
}

export interface ISendConfig{
    hardware: Hardware | undefined;
    header: HeaderFormart | undefined;
    body: BodyType | undefined;
    ip: string;
    port: number | undefined;
    eps : number | undefined;
    sendMode : number | undefined;
}

export enum Hardware {
    firewall = 'firewall',
    VPN = 'VPN',
}

export enum HeaderFormart{
    RFC_3164 = 'RFC_3164',
    RFC_5424 = 'RFC_5424'
}

export enum BodyType{
    CEF = 'CEF',
    LEF = 'LEF',
    KEY_VALUE = 'KEY_VALUE',
    APACHE_LOG = 'APACHE_LOG'
}


/// Registries 

export interface IResponseRegistries {
  id: number
  user: User
  hardware: string
  headerType: string
  body: string
  ip: string
  port: string
  eps: string
  sending: string
}

export interface User {
  id: string
  username: string
  role: string
}
