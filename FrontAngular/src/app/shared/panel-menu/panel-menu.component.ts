import { Component, Input } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-panel-menu',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './panel-menu.component.html',
  styleUrl: './panel-menu.component.css'
})
export class PanelMenuComponent {

  @Input() actualPanel : string = '';
  @Input() role : string = '';

  constructor() { }

}
