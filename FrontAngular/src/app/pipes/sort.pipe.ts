import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from '../interfaces/api.interface';

@Pipe({
  name: 'sort',
  standalone: true
})
export class SortPipe implements PipeTransform {

  transform(value: IUser[], property:string , order: string = 'asc'): IUser[] {

    if(!value || value.length === 0){
      return value;
    }
    
    const priorityRoles = {
      'Admin': 1,
      'Mantainer': 2,
      'User': 3,
    }

    

    return value;
  }

}
