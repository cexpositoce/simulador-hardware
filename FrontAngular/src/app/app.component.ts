import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MainComponent } from "./pages/main/main/main.component";
import { UserpanelComponent } from "./pages/userpanel/userpanel.component";
import { AdminpanelComponent } from "./pages/adminpanel/adminpanel.component";


@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
    imports: [RouterOutlet, MainComponent, UserpanelComponent, AdminpanelComponent]
})
export class AppComponent {
  title = 'FrontAngular';
}
